package Modelo;

import Vista.dlgVista;

public class Docentes {

    private int num_Doc = 0;
    private String nombre = "";
    private String domicilio = "";
    private int nivel = 0;
    private float pagoBase = 0;
    private float horasImp = 0;

    public int getNum_Doc() {
        return num_Doc;
    }

    public void setNum_Doc(int num_Doc) {
        this.num_Doc = num_Doc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public float getHorasImp() {
        return horasImp;
    }

    public void setHorasImp(float horasImp) {
        this.horasImp = horasImp;
    }

    public Docentes() {

    }

    public Docentes(Docentes doc) {

    }

    public float Calcular_Impuestos() {

        return (float) (Calcular_Pago() * .16);

    }

    public float Calcular_Pago() {
        float incre = 0.0f;
        switch (nivel) {
            case 0:
                incre = this.pagoBase + (this.pagoBase * 0.30f);
                break;
            case 1:
                incre = this.pagoBase + (this.pagoBase * 0.50f);
                break;
            case 2:
                incre = this.pagoBase;
                break;
        }
        return incre * horasImp;
    }

    public float Calcular_Impuesto() {
        return this.Calcular_Pago() * 0.16f;
    }

    public float Calcular_Bono(int cantHijos) {
        float calculo = 0.0f;
        if (cantHijos >= 1 && cantHijos <= 2) {
            calculo = 0.05f * this.Calcular_Pago();
        } else if (cantHijos >= 3 && cantHijos <= 5) {
            calculo = this.Calcular_Pago() * 0.10f;
        } else if (cantHijos > 5) {
            calculo = this.Calcular_Pago() * 0.20f;
        }
        return calculo;
    }
}
