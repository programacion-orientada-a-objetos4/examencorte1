package Controlador;

import Modelo.Docentes;
import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    public Controlador(Docentes doc, dlgVista vista) {
        this.vista = vista;
        this.docentes = doc;
    
        this.vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnlimpiar.addActionListener(this);
        vista.cmbNivel.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.txtPagoHoraBase.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.cmbNivel.setEnabled(false);
        vista.spnHijos.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.btnCancelar.setEnabled(false);
        vista.txtDomicilio.setEnabled(false);
        vista.txtPgoHorasIm.setEnabled(false);
        vista.btnlimpiar.setEnabled(false);
        vista.btnMostrar.setEnabled(false);
        vista.btnCerrar.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtNumDoc.setEnabled(false);
    }

    private Docentes docentes;
    private dlgVista vista;

    private void iniciarVista() {
        vista.setVisible(true);
        vista.setSize(730, 530);
        vista.setTitle("Docentes");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // btn nuevo
        if (e.getSource() == vista.btnNuevo) {
            vista.txtPagoHoraBase.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.cmbNivel.setEnabled(true);
            vista.spnHijos.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtPgoHorasIm.setEnabled(true);
            vista.btnlimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
            vista.txtNombre.setEnabled(true);
             vista.txtNumDoc.setEnabled(true);
        }
        // btn cerrar
        if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir?",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        if (e.getSource() == vista.btnlimpiar) {
            limpiarCampos();
        }
        if (e.getSource() == vista.btnCancelar) {
            limpiarCampos();
            vista.btnCancelar.setEnabled(false);
            vista.cmbNivel.setEnabled(false);
            vista.spnHijos.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.txtHorasImp.setEnabled(false);
            vista.btnlimpiar.setEnabled(false);
        }
        if (e.getSource() == vista.btnGuardar) {
            try {
                docentes.setDomicilio(vista.txtDomicilio.getText());
                docentes.setNombre(vista.txtNombre.getText());
                docentes.setNivel(vista.cmbNivel.getSelectedIndex());
                docentes.setNum_Doc(Integer.parseInt(vista.txtNumDoc.getText()));
                docentes.setHorasImp(Float.parseFloat(vista.txtHorasImp.getText()));
                docentes.setPagoBase(Float.parseFloat(vista.txtPagoHoraBase.getText()));

                JOptionPane.showMessageDialog(vista, "Guardado con éxito");
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: Ingresa valores numéricos válidos",
                        "Error de formato", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getSource() == vista.btnMostrar) {
            try {
                float x, y, z;
                x = docentes.Calcular_Impuestos();
                y = docentes.Calcular_Bono((int) vista.spnHijos.getValue());
                z = docentes.Calcular_Pago();
                vista.txtPgoHorasIm.setText(String.valueOf(docentes.Calcular_Pago()));
                vista.txtDomicilio.setText(String.valueOf(docentes.getDomicilio()));
                vista.txtNumDoc.setText(String.valueOf(docentes.getNum_Doc()));
                vista.txtNombre.setText(String.valueOf(docentes.getNombre()));
                vista.txtHorasImp.setText(String.valueOf(docentes.getHorasImp()));
                vista.txtPagoHoraBase.setText(String.valueOf(docentes.getPagoBase()));
                vista.txtPagoBono.setText(String.valueOf(docentes.Calcular_Bono(Integer.parseInt(vista.spnHijos.getValue().toString()))));
                vista.txtDescuentoXImp.setText(String.valueOf((long) docentes.Calcular_Impuestos()));
                vista.txtTotalPago.setText(String.valueOf(z - x + y));
                vista.txtPagoHoraBase.setEnabled(false);
                vista.txtNombre.setEnabled(false);
                vista.txtDomicilio.setEnabled(false);
                vista.txtHorasImp.setEnabled(false);
                vista.txtNumDoc.setEnabled(false);
                vista.txtPagoBono.setEnabled(false);
                vista.txtDescuentoXImp.setEnabled(false);
                vista.txtTotalPago.setEnabled(false);
                vista.txtPgoHorasIm.setEnabled(false);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: Ingresa valores numéricos válidos",
                        "Error de formato", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void limpiarCampos() {
        vista.txtPagoHoraBase.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.txtHorasImp.setText("");
        vista.txtNumDoc.setText("");
        vista.txtPagoBono.setText("");
        vista.txtDescuentoXImp.setText("");
        vista.txtTotalPago.setText("");
        vista.txtPgoHorasIm.setText("");
    }

    public static void main(String[] args) {
        Docentes doc = new Docentes();
        dlgVista vista = new dlgVista(new JFrame(), true);
        Controlador contra = new Controlador(doc, vista);
        contra.iniciarVista();
    }
}
